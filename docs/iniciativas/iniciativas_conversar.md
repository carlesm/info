# Conversación y comunicación

Todas las iniciativas de conversación

## Grupos de Telegram

Como un intento por estar coordinados:

* Busca el grupo de telegram más cercano a tu ubicación en el [listado oficial](https://www.coronavirusmakers.org/index.php/es/17-telegram).
* Lee el mensaje anclado y sigue las normas del grupo. Pueden variar entre un grupo y otro.
* Una vez dentro usa el canal para estar informado y procura meter el menor ruido posible:
  * Si tienes alguna duda, usa la búsqueda de telegram y trata de encontrar información al respecto.
  * Resolver dudas de otros es más interesante que preguntar.
  * Los mensajes del tipo "Me gustaría ayudar con algo" son poco eficaces.
  * Procura escribir mensajes de texto en lugar de grabar mensajes de voz.

Algunos (pocos) ejemplos de grupos y sus mensajes anclados

* [Grupo Principal](../grupos_telegram/coronavirus_makers.md)
* [Grupo de Baleares](../grupos_telegram/CV_FAB_BAL.md)

Trata de localizar el grupo más cercano a tu zona geográfica

* Clasificación de [grupos y sus relaciones](https://kumu.io/lahoramaker/coronavirusmakers-nacional#grupos-y-relaciones-con-otras-iniciativas/cv19fabcanalinfo) (tratamos de mantenerlo actualizado)
